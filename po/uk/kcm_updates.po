# Translation of kcm_updates.po to Ukrainian
# Copyright (C) 2020 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcm_updates\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-07 00:45+0000\n"
"PO-Revision-Date: 2022-08-25 09:24+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: kcm/package/contents/ui/main.qml:32
#, kde-format
msgid "Update software:"
msgstr "Оновити програми:"

#: kcm/package/contents/ui/main.qml:33
#, kde-format
msgid "Manually"
msgstr "Вручну"

#: kcm/package/contents/ui/main.qml:43
#, kde-format
msgid "Automatically"
msgstr "Автоматично"

#: kcm/package/contents/ui/main.qml:50
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Software updates will be downloaded automatically when they become "
"available. Updates for applications will be installed immediately, while "
"system updates will be installed the next time the computer is restarted."
msgstr ""
"Оновлення програмного забезпечення буде отримано автоматично, щойно вони "
"з'являться у сховищах. Оновлення програм буде встановлено негайно, а "
"оновлення системи — під час наступного перезавантаження комп'ютера."

#: kcm/package/contents/ui/main.qml:61
#, kde-format
msgctxt "@title:group"
msgid "Update frequency:"
msgstr "Частота оновлення:"

#: kcm/package/contents/ui/main.qml:61
#, kde-format
msgctxt "@title:group"
msgid "Notification frequency:"
msgstr "Частота сповіщення:"

#: kcm/package/contents/ui/main.qml:64
#, kde-format
msgctxt "@item:inlistbox"
msgid "Daily"
msgstr "щодня"

#: kcm/package/contents/ui/main.qml:65
#, kde-format
msgctxt "@item:inlistbox"
msgid "Weekly"
msgstr "щотижня"

#: kcm/package/contents/ui/main.qml:66
#, kde-format
msgctxt "@item:inlistbox"
msgid "Monthly"
msgstr "щомісяця"

#: kcm/package/contents/ui/main.qml:67
#, kde-format
msgctxt "@item:inlistbox"
msgid "Never"
msgstr "ніколи"

#: kcm/package/contents/ui/main.qml:111
#, kde-format
msgid "Use offline updates:"
msgstr "Використання автономних оновлень:"

#: kcm/package/contents/ui/main.qml:124
#, kde-format
msgid ""
"Offline updates maximize system stability by applying changes while "
"restarting the system. Using this update mode is strongly recommended."
msgstr ""
"Автономні оновлення роблять стабільність роботи системи максимальною шляхом "
"застосування змін під час перезапуску системи. Наполегливо рекомендуємо "
"скористатися цим режимом оновлення."

#: kcm/updates.cpp:31
#, kde-format
msgid "Software Update"
msgstr "Оновлення програм"

#: kcm/updates.cpp:33
#, kde-format
msgid "Configure software update settings"
msgstr "Налаштовування параметрів оновлення програмного забезпечення"
