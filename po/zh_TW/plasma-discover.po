# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin Weng <franklin@goodhorse.idv.tw>, 2012, 2013, 2014, 2015.
# Jeff Huang <s8321414@gmail.com>, 2016, 2017, 2020.
# pan93412 <pan93412@gmail.com>, 2018, 2019, 2020.
# Chaoting Liu <brli@chakralinux.org>, 2021.
# Kisaragi Hiu <mail@kisaragi-hiu.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-14 00:57+0000\n"
"PO-Revision-Date: 2022-07-02 20:23+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Traditional Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.04.2\n"

#: discover/DiscoverObject.cpp:159
#, kde-format
msgid ""
"Discover currently cannot be used to install any apps or perform system "
"updates because none of its app backends are available."
msgstr ""
"由於沒有可用的應用程式後端，Discover 目前無法安裝應用程式或進行系統更新。"

#: discover/DiscoverObject.cpp:163
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can install some on the Settings page, under the <interface>Missing "
"Backends</interface> section.<nl/><nl/>Also please consider reporting this "
"as a packaging issue to your distribution."
msgstr ""
"您可以至設定頁面中<interface>缺失的後端介面</interface>區塊下進行安裝。同時也"
"敬請考慮將此情況向您的發行版回報為軟體包打包問題。"

#: discover/DiscoverObject.cpp:168 discover/DiscoverObject.cpp:380
#: discover/qml/UpdatesPage.qml:107
#, kde-format
msgid "Report This Issue"
msgstr "回報此問題"

#: discover/DiscoverObject.cpp:173
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can use <command>pacman</command> to install the optional dependencies "
"that are needed to enable the application backends.<nl/><nl/>Please note "
"that Arch Linux developers recommend using <command>pacman</command> for "
"managing software because the PackageKit backend is not well-integrated on "
"Arch Linux."
msgstr ""
"您可以使用 <command>pacman</command> 來安裝用來啟用應用程式後端介面的可選依"
"賴。<nl/><nl/>請注意，由於 PackageKit 後端在 Arch Linux 上沒有很完整的整合，"
"Arch Linux 開發者建議使用 <command>pacman</command> 來管理軟體。"

#: discover/DiscoverObject.cpp:181
#, kde-format
msgid "Learn More"
msgstr "了解更多"

#: discover/DiscoverObject.cpp:269
#, kde-format
msgid "Could not find category '%1'"
msgstr "找不到分類「%1」"

#: discover/DiscoverObject.cpp:284
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr ""

#: discover/DiscoverObject.cpp:306
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr "無法在無 Flatpak 後端 %1 情況下與 Flatpak 資源互動。請先安裝它。"

#: discover/DiscoverObject.cpp:310
#, kde-format
msgid "Could not open %1"
msgstr "無法開啟 %1"

#: discover/DiscoverObject.cpp:372
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr "請再度確認已安裝 Snap 支援。"

#: discover/DiscoverObject.cpp:374
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr "無法開啟 %1，在可用的軟體庫中找不到它。"

#: discover/DiscoverObject.cpp:377
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr "請向您的發行版的打包者回報此問題。"

#: discover/DiscoverObject.cpp:442 discover/DiscoverObject.cpp:443
#: discover/main.cpp:120 discover/qml/BrowsingPage.qml:20
#, kde-format
msgid "Discover"
msgstr "Discover"

#: discover/DiscoverObject.cpp:443
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr "Discover 在某些工作結束前就被關閉了。正在等待它完成。"

#: discover/main.cpp:42
#, kde-format
msgid "Directly open the specified application by its appstream:// URI."
msgstr "直接透過 appstream:// 協定開啟指定的應用程式。"

#: discover/main.cpp:43
#, kde-format
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr "搜尋後，以與本 MIME 檔案型態關聯的應用程式開啟。"

#: discover/main.cpp:44
#, kde-format
msgid "Display a list of entries with a category."
msgstr "依類別顯示項目清單。"

#: discover/main.cpp:45
#, kde-format
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr "以指定模式開啟 Discover 軟體中心。模式與工作列按鍵相關。"

#: discover/main.cpp:46
#, kde-format
msgid "List all the available modes."
msgstr "列出所有可用的模式。"

#: discover/main.cpp:47
#, kde-format
msgid "Compact Mode (auto/compact/full)."
msgstr "簡潔模式（自動/簡潔/完整）。"

#: discover/main.cpp:48
#, kde-format
msgid "Local package file to install"
msgstr "要安裝的本地軟體包"

#: discover/main.cpp:49
#, kde-format
msgid "List all the available backends."
msgstr "列出所有可用的後端介面。"

#: discover/main.cpp:50
#, kde-format
msgid "Search string."
msgstr "搜尋字串。"

#: discover/main.cpp:51
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "列出能用於使用者回饋的選項"

#: discover/main.cpp:53
#, kde-format
msgid "Supports appstream: url scheme"
msgstr "支援 appstream：網址機制"

#: discover/main.cpp:122
#, kde-format
msgid "An application explorer"
msgstr "探索更多應用程式"

#: discover/main.cpp:124
#, kde-format
msgid "© 2010-2022 Plasma Development Team"
msgstr "© 2010-2022 Plasma 開發團隊"

#: discover/main.cpp:125
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: discover/main.cpp:126
#, kde-format
msgid "Nate Graham"
msgstr "Nate Graham"

#: discover/main.cpp:127
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr "品質保證、設計與可用性"

#: discover/main.cpp:131
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr "Dan Leinir Turthra Jensen"

#: discover/main.cpp:132
#, kde-format
msgid "KNewStuff"
msgstr "KNewStuff"

#: discover/main.cpp:139
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "pan93412, Chaoting Liu"

#: discover/main.cpp:139
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "pan93412@gmail.com, brli@chakralinux.org"

#: discover/main.cpp:152
#, kde-format
msgid "Available backends:\n"
msgstr "可用的後端介面：\n"

#: discover/main.cpp:208
#, kde-format
msgid "Available modes:\n"
msgstr "可用的模式：\n"

#: discover/qml/AddonsView.qml:18 discover/qml/navigation.js:43
#, kde-format
msgid "Addons for %1"
msgstr "%1 的附加功能"

#: discover/qml/AddonsView.qml:50
#, kde-format
msgid "More…"
msgstr "更多…"

#: discover/qml/AddonsView.qml:59
#, kde-format
msgid "Apply Changes"
msgstr "套用變更"

#: discover/qml/AddonsView.qml:66
#, kde-format
msgid "Reset"
msgstr "重設"

#: discover/qml/AddSourceDialog.qml:20
#, kde-format
msgid "Add New %1 Repository"
msgstr "新增 %1 軟體庫"

#: discover/qml/AddSourceDialog.qml:44
#, kde-format
msgid "Add"
msgstr "新增"

#: discover/qml/AddSourceDialog.qml:49 discover/qml/DiscoverWindow.qml:257
#: discover/qml/InstallApplicationButton.qml:41
#: discover/qml/ProgressView.qml:104 discover/qml/SourcesPage.qml:179
#: discover/qml/UpdatesPage.qml:253 discover/qml/WebflowDialog.qml:39
#, kde-format
msgid "Cancel"
msgstr "取消"

#: discover/qml/ApplicationDelegate.qml:140
#: discover/qml/ApplicationPage.qml:208
#, kde-format
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "%1 個評分"

#: discover/qml/ApplicationDelegate.qml:140
#: discover/qml/ApplicationPage.qml:208
#, kde-format
msgid "No ratings yet"
msgstr "目前還沒有評分"

#: discover/qml/ApplicationPage.qml:62
#, kde-format
msgid "Sources"
msgstr "來源"

#: discover/qml/ApplicationPage.qml:73
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ApplicationPage.qml:185
#, kde-format
msgid "Unknown author"
msgstr "未知的作者"

#: discover/qml/ApplicationPage.qml:220
#, kde-format
msgid "Could not access the screenshots"
msgstr ""

#: discover/qml/ApplicationPage.qml:279
#, kde-format
msgid "Version"
msgstr "版本"

#: discover/qml/ApplicationPage.qml:306
#: discover/qml/ApplicationsListPage.qml:109
#, kde-format
msgid "Size"
msgstr "大小"

#: discover/qml/ApplicationPage.qml:333
#, kde-format
msgid "Distributed by"
msgstr "發行自"

#: discover/qml/ApplicationPage.qml:360
#, kde-format
msgid "License"
msgid_plural "Licenses"
msgstr[0] "授權"

#: discover/qml/ApplicationPage.qml:369
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr "未知"

#: discover/qml/ApplicationPage.qml:407
#, kde-format
msgid "What does this mean?"
msgstr "這代表什麼？"

#: discover/qml/ApplicationPage.qml:417
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] "更多…"

#: discover/qml/ApplicationPage.qml:435 discover/qml/ApplicationPage.qml:879
#, fuzzy, kde-format
#| msgid "Enter a rating"
msgid "Content Rating"
msgstr "輸入一個評分"

#: discover/qml/ApplicationPage.qml:445
#, kde-format
msgid "Age: %1+"
msgstr ""

#: discover/qml/ApplicationPage.qml:469
#, fuzzy, kde-format
#| msgid "See more…"
#| msgid_plural "See more…"
msgctxt "@action"
msgid "See details…"
msgstr "更多…"

#: discover/qml/ApplicationPage.qml:568
#, kde-format
msgid "Documentation"
msgstr "說明文件"

#: discover/qml/ApplicationPage.qml:569
#, kde-format
msgid "Read the project's official documentation"
msgstr "閱讀專案的官方文件"

#: discover/qml/ApplicationPage.qml:585
#, kde-format
msgid "Website"
msgstr "網站"

#: discover/qml/ApplicationPage.qml:586
#, kde-format
msgid "Visit the project's website"
msgstr "造訪專案的網站"

#: discover/qml/ApplicationPage.qml:602
#, kde-format
msgid "Addons"
msgstr "附加功能"

#: discover/qml/ApplicationPage.qml:603
#, kde-format
msgid "Install or remove additional functionality"
msgstr "安裝或移除額外功能"

#: discover/qml/ApplicationPage.qml:622
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr ""

#: discover/qml/ApplicationPage.qml:623
#, fuzzy, kde-format
#| msgid "Unable to load applications"
msgid "Send a link for this application"
msgstr "無法載入應用程式"

#: discover/qml/ApplicationPage.qml:639
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr ""

#: discover/qml/ApplicationPage.qml:659
#, kde-format
msgid "What's New"
msgstr "發掘新鮮事"

#: discover/qml/ApplicationPage.qml:689
#, fuzzy, kde-format
#| msgid "Reviews for %1"
msgid "Loading reviews for %1"
msgstr "%1 的評論"

#: discover/qml/ApplicationPage.qml:695
#, kde-format
msgid "Reviews"
msgstr "評論"

#: discover/qml/ApplicationPage.qml:733
#, kde-format
msgid "Show all %1 Reviews"
msgid_plural "Show all %1 Reviews"
msgstr[0] "顯示所有評論（共 %1 條）"

#: discover/qml/ApplicationPage.qml:745
#, kde-format
msgid "Write a Review"
msgstr "撰寫評論"

#: discover/qml/ApplicationPage.qml:745
#, kde-format
msgid "Install to Write a Review"
msgstr "安裝以撰寫評論"

#: discover/qml/ApplicationPage.qml:757
#, kde-format
msgid "Get Involved"
msgstr "參與"

#: discover/qml/ApplicationPage.qml:790
#, kde-format
msgid "Donate"
msgstr "贊助"

#: discover/qml/ApplicationPage.qml:791
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr "贊助專案來向開發者表達支持與感謝"

#: discover/qml/ApplicationPage.qml:807
#, kde-format
msgid "Report Bug"
msgstr "回報問題"

#: discover/qml/ApplicationPage.qml:808
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr "記錄您找到的問題來幫助它被修復"

#: discover/qml/ApplicationPage.qml:822
#, fuzzy, kde-format
#| msgid "Contribute…"
msgid "Contribute"
msgstr "貢獻…"

#: discover/qml/ApplicationPage.qml:823
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr ""

#: discover/qml/ApplicationPage.qml:848
#, kde-format
msgid "All Licenses"
msgstr "所有授權"

#: discover/qml/ApplicationPage.qml:893
#, kde-format
msgid "Risks of proprietary software"
msgstr "私有軟體的風險"

#: discover/qml/ApplicationPage.qml:899
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""
"本應用程式的原始碼一部分或完全封閉於公開檢視及改善。這代表著第三方與包含您在"
"內的使用者無法檢查它的動作、安全性與可靠性，作者沒有書面同意時也無法修改它或"
"進行重新發佈。<nl/><nl/>本應用程式可能完全是安全的，也有可能侵犯您的權益 —— "
"例如不當收集您的個人資料、記錄您的所在地，或是把您的檔案內容傳給別人。專有軟"
"體要進行確認較為困難，所以您應該只在完全信任它的作者 —— <link url='%1'>%2</"
"link> —— 時才安裝本應用程式。<nl/><nl/>您可以至 <link url='%3'>%3</link> 了解"
"更多。"

#: discover/qml/ApplicationPage.qml:900
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""
"本應用程式的原始碼一部分或完全封閉於公開檢視及改善。這代表著第三方與包含您在"
"內的使用者無法檢查它的動作、安全性與可靠性，作者沒有書面同意時也無法修改它或"
"進行重新發佈。<nl/><nl/>本應用程式可能完全是安全的，也有可能侵犯您的權益 —— "
"例如不當收集您的個人資料、記錄您的所在地，或是把您的檔案內容傳給別人。專有軟"
"體要進行確認較為困難，所以您應該只在完全信任它的作者 —— %1 —— 時才安裝本應用"
"程式。<nl/><nl/>您可以至 <link url='%2'>%2</link> 了解更多。"

#: discover/qml/ApplicationsListPage.qml:53
#, fuzzy, kde-format
#| msgid "Search: %1 - %2 items"
msgid "Search: %2 - %1 item"
msgid_plural "Search: %2 - %1 items"
msgstr[0] "搜尋：%1 - %2 項"

#: discover/qml/ApplicationsListPage.qml:55
#, kde-format
msgid "Search: %1"
msgstr "搜尋：%1"

#: discover/qml/ApplicationsListPage.qml:59
#, fuzzy, kde-format
#| msgid "%1 - %2 items"
msgid "%2 - %1 item"
msgid_plural "%2 - %1 items"
msgstr[0] "%1 - %2 項"

#: discover/qml/ApplicationsListPage.qml:65
#, fuzzy, kde-format
#| msgid "Search - %1 items"
msgid "Search - %1 item"
msgid_plural "Search - %1 items"
msgstr[0] "搜尋：%1 項"

#: discover/qml/ApplicationsListPage.qml:67
#: discover/qml/ApplicationsListPage.qml:225
#, kde-format
msgid "Search"
msgstr "搜尋"

#: discover/qml/ApplicationsListPage.qml:88
#, kde-format
msgid "Sort: %1"
msgstr "排序：%1"

#: discover/qml/ApplicationsListPage.qml:91
#, kde-format
msgid "Name"
msgstr "名稱"

#: discover/qml/ApplicationsListPage.qml:100
#, kde-format
msgid "Rating"
msgstr "評分"

#: discover/qml/ApplicationsListPage.qml:118
#, kde-format
msgid "Release Date"
msgstr "釋出日期"

#: discover/qml/ApplicationsListPage.qml:173
#, kde-format
msgid "Nothing found"
msgstr "找不到東西"

#: discover/qml/ApplicationsListPage.qml:180
#, kde-format
msgctxt "@action:button"
msgid "Search in All Categories"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:189
#, fuzzy, kde-format
#| msgctxt "%1 is the name of an application"
#| msgid "Search the web for \"%1\""
msgctxt "@action:button %1 is the name of an application"
msgid "Search the Web for \"%1\""
msgstr "在網路上搜尋 %1"

#: discover/qml/ApplicationsListPage.qml:193
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr "https://duckduckgo.com/?q=%1"

#: discover/qml/ApplicationsListPage.qml:204
#, fuzzy, kde-format
#| msgctxt "%1 is the name of an application"
#| msgid "\"%1\" was not found in the available sources"
msgctxt ""
"@info:placeholder %1 is the name of an application; %2 is the name of a "
"category of apps or add-ons"
msgid "\"%1\" was not found in the \"%2\" category"
msgstr "可用的來源中找不到 %1"

#: discover/qml/ApplicationsListPage.qml:206
#, fuzzy, kde-format
#| msgctxt "%1 is the name of an application"
#| msgid "\"%1\" was not found in the available sources"
msgctxt "@info:placeholder %1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr "可用的來源中找不到 %1"

#: discover/qml/ApplicationsListPage.qml:207
#, fuzzy, kde-format
#| msgctxt "%1 is the name of an application"
#| msgid ""
#| "\"%1\" may be available on the web. Software acquired from the web has "
#| "not been reviewed by your distributor for functionality or stability. Use "
#| "with caution."
msgctxt "@info:placeholder%1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""
"網路上其他來源可能有提供 %1，網路上下載的軟體未經過發行者查核其功能和穩定性，"
"請小心使用。"

#: discover/qml/ApplicationsListPage.qml:240
#, kde-format
msgid "Still looking…"
msgstr "正在搜尋中..."

#: discover/qml/BrowsingPage.qml:49
#, kde-format
msgid "Unable to load applications"
msgstr "無法載入應用程式"

#: discover/qml/BrowsingPage.qml:88
#, kde-format
msgctxt "@title:group"
msgid "Most Popular"
msgstr ""

#: discover/qml/BrowsingPage.qml:106
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr ""

#: discover/qml/BrowsingPage.qml:120
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr ""

#: discover/qml/BrowsingPage.qml:139 discover/qml/BrowsingPage.qml:168
#, fuzzy, kde-format
#| msgid "See more…"
#| msgid_plural "See more…"
msgctxt "@action:button"
msgid "See More"
msgstr "更多…"

#: discover/qml/BrowsingPage.qml:149
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr ""

#: discover/qml/DiscoverWindow.qml:43
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr "不鼓勵且不必要以 <em>root</em> 身份執行。"

#: discover/qml/DiscoverWindow.qml:56
#, kde-format
msgid "&Home"
msgstr "首頁 (&H)："

#: discover/qml/DiscoverWindow.qml:66
#, kde-format
msgid "&Search"
msgstr "搜尋 (&S)"

#: discover/qml/DiscoverWindow.qml:74
#, kde-format
msgid "&Installed"
msgstr "已安裝 (&I)"

#: discover/qml/DiscoverWindow.qml:81
#, kde-format
msgid "Fetching &updates…"
msgstr "正在下載更新 (&u)…"

#: discover/qml/DiscoverWindow.qml:81
#, kde-format
msgid "&Up to date"
msgstr "已最新 (&U)"

#: discover/qml/DiscoverWindow.qml:81
#, kde-format
msgctxt "Update section name"
msgid "&Update (%1)"
msgstr "可更新項目 (%1) (&U)"

#: discover/qml/DiscoverWindow.qml:88
#, kde-format
msgid "&About"
msgstr "關於 (&A)"

#: discover/qml/DiscoverWindow.qml:96
#, kde-format
msgid "S&ettings"
msgstr "設定 (&E)"

#: discover/qml/DiscoverWindow.qml:146 discover/qml/DiscoverWindow.qml:325
#: discover/qml/DiscoverWindow.qml:432
#, kde-format
msgid "Error"
msgstr "錯誤"

#: discover/qml/DiscoverWindow.qml:150
#, kde-format
msgid "Unable to find resource: %1"
msgstr "找不到資源：%1"

#: discover/qml/DiscoverWindow.qml:244 discover/qml/SourcesPage.qml:169
#, kde-format
msgid "Proceed"
msgstr "繼續"

#: discover/qml/DiscoverWindow.qml:301
#, kde-format
msgid "Report this issue"
msgstr "回報此問題"

#: discover/qml/DiscoverWindow.qml:325
#, fuzzy, kde-format
#| msgid "Votes: %1 out of %2"
msgid "Error %1 of %2"
msgstr "投票：%2 中之 %1"

#: discover/qml/DiscoverWindow.qml:370
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr ""

#: discover/qml/DiscoverWindow.qml:383
#, fuzzy, kde-format
#| msgid "Show contents"
msgctxt "@action:button"
msgid "Show Next"
msgstr "顯示內容"

#: discover/qml/DiscoverWindow.qml:399 discover/qml/UpdatesPage.qml:98
#, kde-format
msgid "Copy to Clipboard"
msgstr "複製至剪貼簿"

#: discover/qml/Feedback.qml:12
#, kde-format
msgid "Submit usage information"
msgstr "上傳使用量資訊"

#: discover/qml/Feedback.qml:13
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""
"傳送匿名使用資訊給 KDE 團隊，以讓我們更貼近使用者的需求。請瀏覽 https://kde."
"org/privacypolicy-apps.php 了解更多。"

#: discover/qml/Feedback.qml:17
#, kde-format
msgid "Submitting usage information…"
msgstr "上傳使用量資訊…"

#: discover/qml/Feedback.qml:17
#, kde-format
msgid "Configure"
msgstr "設定"

#: discover/qml/Feedback.qml:21
#, kde-format
msgid "Configure feedback…"
msgstr "設定回報等級…"

#: discover/qml/Feedback.qml:28 discover/qml/SourcesPage.qml:19
#, kde-format
msgid "Configure Updates…"
msgstr "設定更新…"

#: discover/qml/Feedback.qml:58
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr "您可以藉由分享統計資料及參與問卷調查以協助我們改善這個應用程式。"

#: discover/qml/Feedback.qml:58
#, kde-format
msgid "Contribute…"
msgstr "貢獻…"

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "We are looking for your feedback!"
msgstr "我們期待您的回饋！"

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "Participate…"
msgstr "參與…"

#: discover/qml/InstallApplicationButton.qml:25
#, kde-format
msgctxt "State being fetched"
msgid "Loading…"
msgstr "載入中…"

#: discover/qml/InstallApplicationButton.qml:28
#, kde-format
msgid "Install"
msgstr "安裝"

#: discover/qml/InstallApplicationButton.qml:30
#, kde-format
msgid "Remove"
msgstr "移除"

#: discover/qml/InstalledPage.qml:15
#, kde-format
msgid "Installed"
msgstr "已安裝"

#: discover/qml/navigation.js:19
#, kde-format
msgid "Resources for '%1'"
msgstr "%1 的資源"

#: discover/qml/ProgressView.qml:17
#, kde-format
msgid "Tasks (%1%)"
msgstr "工作排程 (%1%)"

#: discover/qml/ProgressView.qml:17 discover/qml/ProgressView.qml:40
#, kde-format
msgid "Tasks"
msgstr "工作排程"

#: discover/qml/ProgressView.qml:97
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr "%1 - %2: %3, 剩下 %4"

#: discover/qml/ProgressView.qml:98
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:99
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ReviewDelegate.qml:61
#, kde-format
msgid "unknown reviewer"
msgstr "未知的評論者"

#: discover/qml/ReviewDelegate.qml:62
#, kde-format
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b> 由 %2"

#: discover/qml/ReviewDelegate.qml:62
#, kde-format
msgid "Comment by %1"
msgstr "來自 %1 的註解"

#: discover/qml/ReviewDelegate.qml:80
#, kde-format
msgid "Version: %1"
msgstr "版本：%1"

#: discover/qml/ReviewDelegate.qml:80
#, kde-format
msgid "Version: unknown"
msgstr "版本：未知"

#: discover/qml/ReviewDelegate.qml:95
#, kde-format
msgid "Votes: %1 out of %2"
msgstr "投票：%2 中之 %1"

#: discover/qml/ReviewDelegate.qml:102
#, kde-format
msgid "Was this review useful?"
msgstr "本評論實用嗎？"

#: discover/qml/ReviewDelegate.qml:114
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr "有用"

#: discover/qml/ReviewDelegate.qml:131
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr "沒用"

#: discover/qml/ReviewDialog.qml:18
#, kde-format
msgid "Reviewing %1"
msgstr "正在評論 %1"

#: discover/qml/ReviewDialog.qml:24
#, kde-format
msgid "Submit review"
msgstr "提交評論"

#: discover/qml/ReviewDialog.qml:37
#, kde-format
msgid "Rating:"
msgstr "評分："

#: discover/qml/ReviewDialog.qml:42
#, kde-format
msgid "Name:"
msgstr "名稱："

#: discover/qml/ReviewDialog.qml:50
#, kde-format
msgid "Title:"
msgstr "標題："

#: discover/qml/ReviewDialog.qml:67
#, kde-format
msgid "Enter a rating"
msgstr "輸入一個評分"

#: discover/qml/ReviewDialog.qml:68
#, kde-format
msgid "Write the title"
msgstr "撰寫標題"

#: discover/qml/ReviewDialog.qml:69
#, kde-format
msgid "Write the review"
msgstr "撰寫評論"

#: discover/qml/ReviewDialog.qml:70
#, kde-format
msgid "Keep writing…"
msgstr "撰寫中…"

#: discover/qml/ReviewDialog.qml:71
#, kde-format
msgid "Too long!"
msgstr "太長了！"

#: discover/qml/ReviewDialog.qml:72
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr ""

#: discover/qml/ReviewsPage.qml:42
#, kde-format
msgid "Reviews for %1"
msgstr "%1 的評論"

#: discover/qml/ReviewsPage.qml:53
#, kde-format
msgid "Write a Review…"
msgstr "撰寫評論…"

#: discover/qml/ReviewsPage.qml:58
#, kde-format
msgid "Install this app to write a review"
msgstr "安裝這個應用程式來撰寫評論"

#: discover/qml/SearchField.qml:24
#, kde-format
msgid "Search…"
msgstr "搜尋…"

#: discover/qml/SearchField.qml:24
#, kde-format
msgid "Search in '%1'…"
msgstr "在 %1 中搜尋…"

#: discover/qml/SourcesPage.qml:13
#, kde-format
msgid "Settings"
msgstr "設定"

#: discover/qml/SourcesPage.qml:93
#, kde-format
msgid "Default source"
msgstr "預設來源"

#: discover/qml/SourcesPage.qml:100
#, kde-format
msgid "Add Source…"
msgstr "新增來源…"

#: discover/qml/SourcesPage.qml:124
#, kde-format
msgid "Make default"
msgstr "設為預設值"

#: discover/qml/SourcesPage.qml:210
#, kde-format
msgid "Increase priority"
msgstr "增加優先度"

#: discover/qml/SourcesPage.qml:216
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr "無法增加「%1」設定"

#: discover/qml/SourcesPage.qml:221
#, kde-format
msgid "Decrease priority"
msgstr "降低優先度"

#: discover/qml/SourcesPage.qml:227
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr "無法移除「%1」設定"

#: discover/qml/SourcesPage.qml:232
#, kde-format
msgid "Remove repository"
msgstr "移除軟體庫"

#: discover/qml/SourcesPage.qml:243
#, kde-format
msgid "Show contents"
msgstr "顯示內容"

#: discover/qml/SourcesPage.qml:282
#, kde-format
msgid "Missing Backends"
msgstr "未發現後端"

#: discover/qml/UpdatesPage.qml:12
#, kde-format
msgid "Updates"
msgstr "更新"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Update Issue"
msgstr "更新時遇到問題"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Technical details"
msgstr "技術細節"

#: discover/qml/UpdatesPage.qml:61
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr "安裝更新時遇到問題，請稍後重試"

#: discover/qml/UpdatesPage.qml:67
#, kde-format
msgid "See Technical Details"
msgstr "檢視技術細節"

#: discover/qml/UpdatesPage.qml:83
#, kde-format
msgid ""
"If you would like to report the update issue to your distribution's "
"packagers, include this information:"
msgstr "如果您要向發行版的打包者回報此更新問題，請附上以下資訊："

#: discover/qml/UpdatesPage.qml:102
#, kde-format
msgid "Error message copied to clipboard"
msgstr "錯誤訊息已複製到剪貼簿"

#: discover/qml/UpdatesPage.qml:134
#, kde-format
msgid "Update Selected"
msgstr "更新選取的"

#: discover/qml/UpdatesPage.qml:134
#, kde-format
msgid "Update All"
msgstr "全部更新"

#: discover/qml/UpdatesPage.qml:174
#, kde-format
msgid "Ignore"
msgstr ""

#: discover/qml/UpdatesPage.qml:220
#, kde-format
msgid "Select All"
msgstr "全部選取"

#: discover/qml/UpdatesPage.qml:228
#, kde-format
msgid "Select None"
msgstr "全部不選"

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Restart automatically after update has completed"
msgstr "更新完成後自動重新啟動電腦"

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Total size: %1"
msgstr "總計大小：%1"

#: discover/qml/UpdatesPage.qml:278
#, kde-format
msgid "Restart Now"
msgstr "立刻重新開機"

#: discover/qml/UpdatesPage.qml:377
#, kde-format
msgid "%1"
msgstr "%1"

#: discover/qml/UpdatesPage.qml:393
#, kde-format
msgid "Installing"
msgstr "安裝中…"

#: discover/qml/UpdatesPage.qml:428
#, kde-format
msgid "Update from:"
msgstr "更新自："

#: discover/qml/UpdatesPage.qml:440
#, kde-format
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: discover/qml/UpdatesPage.qml:447
#, kde-format
msgid "More Information…"
msgstr "更多資訊…"

#: discover/qml/UpdatesPage.qml:475
#, kde-format
msgctxt "@info"
msgid "Fetching updates…"
msgstr "正在下載更新…"

#: discover/qml/UpdatesPage.qml:488
#, kde-format
msgctxt "@info"
msgid "Updates"
msgstr "更新"

#: discover/qml/UpdatesPage.qml:498
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr "重新啟動電腦以完成更新"

#: discover/qml/UpdatesPage.qml:510 discover/qml/UpdatesPage.qml:517
#: discover/qml/UpdatesPage.qml:524
#, kde-format
msgctxt "@info"
msgid "Up to date"
msgstr "最新"

#: discover/qml/UpdatesPage.qml:531
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr "應該檢查更新"

#: discover/qml/UpdatesPage.qml:538
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr "無法得知最近的一次更新時間"

#~ msgid "Featured"
#~ msgstr "特色應用程式"

#~ msgid "Updates are available"
#~ msgstr "有可用的更新"

#~ msgctxt "Short for 'show updates'"
#~ msgid "Show"
#~ msgstr "顯示全部"

#, fuzzy
#~| msgid "More..."
#~ msgid "Show more..."
#~ msgstr "更多..."

#~ msgid "proprietary"
#~ msgstr "私有"

#, fuzzy
#~| msgid "Could not close Discover, there are tasks that need to be done."
#~ msgid "Could not close Discover because some tasks are still in progress."
#~ msgstr "無法關閉 Discover，仍有未完成的工作排程。"

#~ msgid "Quit Anyway"
#~ msgstr "仍然關閉"

#~ msgid "Loading…"
#~ msgstr "載入中..."

#~ msgid "Return to the Featured page"
#~ msgstr "回到「特色應用程式」頁面"

#~ msgid "Category:"
#~ msgstr "分類："

#~ msgid "Author:"
#~ msgstr "作者："

#~ msgid "Size:"
#~ msgstr "大小："

#~ msgid "Source:"
#~ msgstr "來源："

#~ msgid "See full license terms"
#~ msgstr "檢視完整授權條款"

#~ msgid "Read the user guide"
#~ msgstr "閱讀使用者指南"

#~ msgid "Make a donation"
#~ msgstr "贊助我們"

#~ msgid "Report a problem"
#~ msgstr "回報問題"

#~ msgid "%1 (Default)"
#~ msgstr "%1（預設值）"

#~ msgid "Extensions…"
#~ msgstr "擴充元件..."

#~ msgid "Please verify Internet connectivity"
#~ msgstr "請檢查網路連線狀態"

#~ msgid "All updates selected (%1)"
#~ msgstr "已選取全部更新 (%1)"

#~ msgid "%1/%2 update selected (%3)"
#~ msgid_plural "%1/%2 updates selected (%3)"
#~ msgstr[0] "已選取 %1（共 %2）個更新 (%3)"

#~ msgid "OK"
#~ msgstr "確定"

#~ msgctxt "Part of a string like this: '<app name> - <category>'"
#~ msgid "- %1"
#~ msgstr "- %1"

#~ msgctxt "Install the version of an app that comes from Snap, Flatpak, etc"
#~ msgid "Install from %1"
#~ msgstr "從 %1 安裝"

#~ msgctxt "@info"
#~ msgid "The updates will be installed the next time the system is restarted"
#~ msgstr "以下更新將在系統下次重新啟動後安裝"

#~ msgctxt "@info"
#~ msgid "The system must be restarted to fully apply the installed updates"
#~ msgstr "系統需重新啟動才能完整套用更新"

#~ msgid "%1, released on %2"
#~ msgstr "%1，於 %2 釋出"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "State being fetched"
#~ msgid "Loading..."
#~ msgstr "載入中..."

#~ msgid "Write a Review..."
#~ msgstr "撰寫評論…"

#~ msgid "Search..."
#~ msgstr "搜尋..."

#~ msgctxt "@info"
#~ msgid "It is unknown when the last check for updates was"
#~ msgstr "不知道上次檢查更新的時間"

#~ msgid "No application back-ends found, please report to your distribution."
#~ msgstr "找不到應用程式後端，請回報此問題到您的散佈版。"

#~ msgid "Sorry..."
#~ msgstr "抱歉 ..."

#~ msgid "Useful?"
#~ msgstr "有用嗎？"

#~ msgid "Update to version %1"
#~ msgstr "更新至 %1 版"

#~ msgctxt "Do not translate or alter \\x9C"
#~ msgid "%1 → %2%1 → %2%2"
#~ msgstr "%1 → %2%1 → %2%2"

#~ msgid "Delete the origin"
#~ msgstr "刪除來源"

#~ msgid "Write a review!"
#~ msgstr "撰寫一個評論！"

#~ msgid "Be the first to write a review!"
#~ msgstr "當第一個撰寫評論的人吧！"

#~ msgid "Install and be the first to write a review!"
#~ msgstr "安裝後當第一個評論這個程式的人！"

#~ msgid "Jonathan Thomas"
#~ msgstr "Jonathan Thomas"

#~ msgid "Discard"
#~ msgstr "丟棄"

#~ msgid "Hide"
#~ msgstr "隱藏"

#~ msgid "User Guide:"
#~ msgstr "用戶指南："

#~ msgid "Failed to remove the source '%1'"
#~ msgstr "無法移除資源「%1」"

#~ msgctxt "@info"
#~ msgid "Fetching Updates..."
#~ msgstr "正在取得更新…"

#~ msgctxt "@info"
#~ msgid "Up to Date"
#~ msgstr "最新版本"

#~ msgid "<em>Tell us about this review!</em>"
#~ msgstr "<em>告訴我們關於這個評論！</em>"

#~ msgid "<em>%1 out of %2 people found this review useful</em>"
#~ msgstr "<em>%2 個人中的 %1 個覺得這個評論很有用</em>"

#~ msgid ""
#~ "<em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em>"
#~ msgstr ""
#~ "<em>有用嗎？<a href='true'><b>是的</b></a>/<a href='false'>沒用</a></em>"

#~ msgid ""
#~ "<em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em>"
#~ msgstr ""
#~ "<em>有用嗎？<a href='true'>是的</a>/<a href='false'><b>沒用</b></a></em>"

#~ msgid "<em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em>"
#~ msgstr "<em>有用嗎？<a href='true'>是的</a>/<a href='false'>沒用</a></em>"

#~ msgctxt "@info"
#~ msgid "Submission name:<nl/>%1"
#~ msgstr "提交名稱：<nl/>%1"

#~ msgid "Review:"
#~ msgstr "評論："

#~ msgid "Review..."
#~ msgstr "評論 ..."
